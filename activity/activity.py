year = input("Please input a year\n")

if year.isdigit() and int(year) > 0:
	if int(year) % 4 == 0:
		print(f"{year} is a leap year")
	else:
		print(f"{year} is not a leap year")
else:
	print("Please input a proper year")

row = int(input("Enter number of rows\n"))
col = int(input("Enter number of columns\n"))

for x in range(row):
	str = ""
	for y in range(col):
		str += "*"
	print(str)